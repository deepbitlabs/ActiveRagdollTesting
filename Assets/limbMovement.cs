﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class limbMovement : MonoBehaviour 
{
    public HingeJoint leftFoot;
    public HingeJoint rightFoot;
    //public HingeJoint rightHand;
    public HingeJoint Spine;

    public float thrust;
    public Rigidbody rigidbody;
    public Rigidbody rightLeg;
    public Rigidbody leftLeg;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //rightHand.AddRelativeForce(Vector3.up * thrust);

        rigidbody.AddForce(Vector3.up * 85);
        rightLeg.AddForce(Vector3.forward * 85);
        leftLeg.AddForce(Vector3.back * 85);
    }


}
