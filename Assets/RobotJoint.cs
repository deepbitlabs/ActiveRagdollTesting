﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotJoint : MonoBehaviour 
{
	public Rigidbody rigidbody;
	public Transform leg;

	public float pull;

	public Vector3 Axis;
	public Vector3 StartOffset;

	public Vector3 forcePositionOffset;

	bool movement;

	void Start()
	{
		rigidbody = GetComponent<Rigidbody> ();

		StartOffset = transform.localPosition;
	}

//	void ApplyForce(Rigidbody body)
//	{
//		Vector3 direction = body.transform.position - transform.position;
//		body.AddForceAtPosition (direction.normalized, transform.position);
//
//	}

	void Update()
	{
		if (movement) 
		{
			rigidbody.AddForceAtPosition(Vector3.forward * pull, transform.position + forcePositionOffset);
		} 
		else 
		{
			rigidbody.AddForceAtPosition(Vector3.forward * pull, -transform.position + forcePositionOffset);
		}

	}


//	public Vector3 ForwardKinematics (float [] angles)
//	{
//		Vector3 prevPoint = Joints [0].transform.position;
//		Quaternion rotation = Quaternion.identity;
//		for(int i = 1; i < Joints.Length; i++)
//		{
//			rotation *= Quaternion.AngleAxis (angles [i - 1], Joints [i - 1].Axis);
//			Vector3 nextPoint = prevPoint + rotation * Joints [i].StartOffset;
//		}
//		return prevPoint;
//	}
}
